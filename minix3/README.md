### Minix 3 _Throbber_

Program:(https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix3/index.html)

Koden: (https://gitlab.com/sommerpilgaard99/aestetisk-programmering/-/blob/main/minix3/minix3.js)


![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix3/Minix3_billede.jpg)

Describe your throbber design, both conceptually and technically.

* Jeg har designet min throbber til at ligne en CD som snurrer rundt, som den ville se ud idet den bliver læst inde i fx. en pc.
Da jeg designede den havde jeg ikke tænkt at det skulle forestille en CD fra starten, men den begyndte lidt at ligne det omkring halvvejs, derfor skiftede jeg fokuset til at få den til at ligne en CD endnu mere.

* Dette er den længeste kode jeg nogensinde har lavet og jeg var selv overrasket da jeg så at jeg havde 1600 linjer kode, men selvom dette er et meget højt nummer, kan det godt være lidt misvisende hvis man kigge bare lidt mere på hvad der står i min kode. Dette er fordi det mest af alt bare er en simpel kode på omkring 20 linjer jeg har skrevet, jeg derefter har duplikeret og manipuleret for at skabe dette CD mønster.
  * Jeg har lavet en række af roterende ellipser der snurrer i et loop med små ellipser inderst der bliver større ud til uder rotationen. De sorte og rækker bevæger sig hurtigere end de regnbuefarvede, da jeg følte gav et mere æstetisk udseende, som jeg selv var meget stor fan af.
    * Dem som snurrer mod urets retning, altså de regnbuefarvede, er lavet så de starter med en rød ellipse, hvorefter resten af regnbuensfarver bliver mere og mere gennemsigtige indtil vi når til den næste røde ellipse. Dette er gjort så det ligner et evigt forsat loop, selvom der til hver regnbue kun er 5 ellipser.
    * Dem som snurrer med urets retning er i bund og grund opbygget af det samme som regnebuen, men her er der færre loops og mere gennemsigtighed, så man kan se regnbuen mere tydeligt igennem, hvilket giver denne _LaserDisc_ vibe jeg er gået efter.

For jeres egen mentalitets skyld så læs bare kun de første 100 linjers kode, da resten efter dette er gentagene med kun få ændringer. ![Link til sourcecode](https://gitlab.com/sommerpilgaard99/aestetisk-programmering/-/blob/main/minix3/minix3.js)

Jeg ville gerne have forsøgt mig i at bruge en syntakse som _time_, men som man kan se ud fra koden her, var jeg lidt optaget af at få skabt det æstetiske udseende, fremfor den æstetiske tidsmæssige del. Jeg ville have skabt noget som ville have vidst noget, efter et bestemt stykke tid havde gået, nu er det dog bare blevet til throbberen i sig selv.

#### Tid
Jeg har forsøgt at skabe denne CD effekt idet jeg føler meget hurtigt man kan koble den til idéen om tid, at det tager tid for en computer at læse en CD. Det er også opbygget efter et minde fra min egen barndom, hvor vi havde en helt gammel computer, og så vidt jeg kan huske havde den en throbber lignende den jeg har skabt her, når man havde indsat en disc. Så for mig er den en meget god indikatør om tid der går. Jeg kan næsten personligt høre lyden af en computer som læser en disc når jeg  ser på min throbber.
Jeg ved at mange ser throbbere som værende en irritation og forbinder det med at noget tager lang tid, eller at man kan regne med at noget ikke virker som det skal, hvilket jeg også meget godt kan forstå, da det passer meget godt, hvis man mest af alt har brugt teknologi siden 2010 og måske ikke kan huske hvordan det var før i tiden. Som jeg nævnte før har computere været en meget integreret del af mit liv, helt fra dengang jeg var omkring 5 år gammel. Så jeg husker meget tydeligt hvor længe man skulle vente på at noget gad at loade, om det var i brugen af internettet eller hvis man bare skulle spille snake. Hvis man måler disse nye loading tider med de gamle, ligner de ingenting, selv hvis man skal se på en throbber i sådan 30sek, er det intet i forhold til førhen.

Udover alt det jeg har oplevet med throbbers i min fortid, ser jeg dem stadig ofte, og jeg må være ærlig og indrømme at jeg selv har være frustreret flere gange. Selvom jeg ved det ikke hjælper go jeg ved med sikkerhed det ikke fremskynder processen, klikker jeg altid flere gange, hvis noget tager længere tid end det plejer, da jeg bliver irriteret og utålmodig. Jeg synes det er lidt sjovt at som loading tiden på computere og alt andet er blevet hurtigere, er vi som brugere blevet mere utålmodige. Hvor vi før vidste at computeren gør det så hurtigt som muligt, ser vi det nu som om et eller andet er gået galt, hvis vi ser den her lille throbber.



