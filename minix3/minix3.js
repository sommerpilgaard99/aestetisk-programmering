
let minusVinkel = 0;
let minusVinkel1 = 10;
let minusVinkel2 = 20;
let minusVinkel3 = 30;
let minusVinkel4 = 40;
let minusVinkel5 = 50;

let minusVinkel6 = 60;
let minusVinkel7 = 70;
let minusVinkel8 = 80;
let minusVinkel9 = 90;
let minusVinkel10 = 100;
let minusVinkel11 = 110;

let minusVinkel12 = 120;
let minusVinkel13 = 130;
let minusVinkel14 = 140;
let minusVinkel15 = 150;
let minusVinkel16 = 160;
let minusVinkel17 = 170;

let minusVinkel18 = 180;
let minusVinkel19 = 190;
let minusVinkel20 = 200;
let minusVinkel21 = 210;
let minusVinkel22 = 220;
let minusVinkel23 = 230;

let minusVinkel24 = 240;
let minusVinkel25 = 250;
let minusVinkel26 = 260;
let minusVinkel27 = 270;
let minusVinkel28 = 280;
let minusVinkel29 = 290;

let minusVinkel30 = 300;
let minusVinkel31 = 310;
let minusVinkel32 = 320;
let minusVinkel33 = 330;
let minusVinkel34 = 340;
let minusVinkel35 = 350;

let soloVinkel = 150;

let vinkel = 50;
let vinkel1 = 40;
let vinkel2 = 30;
let vinkel3 = 20;
let vinkel4 = 10;
let vinkel5 = 0;

let vinkel6 = 230;
let vinkel7 = 220;
let vinkel8 = 210;
let vinkel9 = 200;
let vinkel10 = 190;
let vinkel11 = 180;

let vinkel12 = 140;
let vinkel13 = 130;
let vinkel14 = 120;
let vinkel15 = 110;
let vinkel16 = 100;
let vinkel17 = 90;

let vinkel18 = 320;
let vinkel19 = 310;
let vinkel20 = 300;
let vinkel21 = 290;
let vinkel22 = 280;
let vinkel23 = 270;

let ellipseDia = 0;

function setup() {
  //create a drawing canvas
  createCanvas(windowWidth, windowHeight);
  background(255);
  angleMode(DEGREES);
 }
 
 function draw() {
  translate(width/2,height/2);




  background(0,0,0, 10);

  noStroke();  

 //minusVinkel23

 push();
 rotate(minusVinkel35);
 fill(150,150,150,50);
 ellipse(74,74,35,35);
 pop();

 push();
 rotate(minusVinkel35);
 fill(150,150,150,50);
 ellipse(52,52, 30, 30);
 pop();

 push();
 rotate(minusVinkel35);
 minusVinkel35 -=4
 fill(150,150,150,50);
 ellipse(18,18,20,20);
 pop();

 push();
 rotate(minusVinkel35);
 fill(150,150,150,50);
 ellipse(33,33, 25, 25);
 pop();



  //minusVinkler
  push();
  rotate(minusVinkel);
  fill(255,179,186);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(minusVinkel);
  fill(255,179,186);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(minusVinkel);
  minusVinkel -=4
  fill(255,179,186);
  ellipse(18,18,20,20);
  pop();

  push();
  rotate(minusVinkel);
  fill(255,179,186);
  ellipse(33,33, 25, 25);
  pop();

  //minusVinusVinkel1
  push();
  rotate(minusVinkel1);
  minusVinkel1 -=4
  fill(255,223,186, 90);
  ellipse(18,18,20,20);
  pop();
  
  push();
  rotate(minusVinkel1);
  fill(255,223,186, 90);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(minusVinkel1);
  fill(255,223,186,90);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(minusVinkel1);
  fill(255,223,186,90);
  ellipse(33,33, 25, 25);
  pop();

  //minusVinkel2
  push();
  rotate(minusVinkel2);
  minusVinkel2 -=4
  fill(255,255,186,80);
  ellipse(18,18,20,20);
  pop();
  
  push();
  rotate(minusVinkel2);
  fill(255,255,186,80);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(minusVinkel2);
  fill(255,255,186, 80);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(minusVinkel2);
  fill(255,255,186, 80);
  ellipse(33,33, 25, 25);
  pop();

  //minusVinkel3
  push();
  rotate(minusVinkel3);
  minusVinkel3 -=4
  fill(186,255,201,70);
  ellipse(18,18,20,20);
  pop();
  
  push();
  rotate(minusVinkel3);
  fill(186,255,201,70);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(minusVinkel3);
  fill(186,255,201, 70);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(minusVinkel3);
  fill(186,255,201, 70);
  ellipse(33,33, 25, 25);
  pop();
 
  //minusVinkel4
  push();
  rotate(minusVinkel4);
  minusVinkel4 -=4
  fill(186,225,255,60);
  ellipse(18,18,20,20);
  pop();
  
  push();
  rotate(minusVinkel4);
  fill(186,225,255,60);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(minusVinkel4);
  fill(186,225,255, 60);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(minusVinkel4);
  fill(186,225,255, 60);
  ellipse(33,33, 25, 25);
  pop();

  //minusVinkel5
  push();
  rotate(minusVinkel5);
  minusVinkel5 -=4
  fill(150,150,150,50);
  ellipse(18,18,20,20);
  pop();
  
  push();
  rotate(minusVinkel5);
  fill(150,150,150,50);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(minusVinkel5);
  fill(150,150,150,50);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(minusVinkel5);
  fill(150,150,150,50);
  ellipse(33,33, 25, 25);
  pop();

  //minusVinkler
  push();
  rotate(minusVinkel6);
  fill(255,179,186);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(minusVinkel6);
  fill(255,179,186);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(minusVinkel6);
  minusVinkel6 -=4
  fill(255,179,186);
  ellipse(18,18,20,20);
  pop();

  push();
  rotate(minusVinkel6);
  fill(255,179,186);
  ellipse(33,33, 25, 25);
  pop();

  //minusVinusVinkel7
  push();
  rotate(minusVinkel7);
  minusVinkel7 -=4
  fill(255,223,186, 90);
  ellipse(18,18,20,20);
  pop();
  
  push();
  rotate(minusVinkel7);
  fill(255,223,186, 90);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(minusVinkel7);
  fill(255,223,186,90);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(minusVinkel7);
  fill(255,223,186,90);
  ellipse(33,33, 25, 25);
  pop();

  //minusVinkel8
  push();
  rotate(minusVinkel8);
  minusVinkel8 -=4
  fill(255,255,186,80);
  ellipse(18,18,20,20);
  pop();
  
  push();
  rotate(minusVinkel8);
  fill(255,255,186,80);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(minusVinkel8);
  fill(255,255,186, 80);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(minusVinkel8);
  fill(255,255,186, 80);
  ellipse(33,33, 25, 25);
  pop();

  //minusVinkel9
  push();
  rotate(minusVinkel9);
  minusVinkel9 -=4
  fill(186,255,201,70);
  ellipse(18,18,20,20);
  pop();
  
  push();
  rotate(minusVinkel9);
  fill(186,255,201,70);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(minusVinkel9);
  fill(186,255,201, 70);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(minusVinkel9);
  fill(186,255,201, 70);
  ellipse(33,33, 25, 25);
  pop();
 
  //minusVinkel10
  push();
  rotate(minusVinkel10);
  minusVinkel10 -=4
  fill(186,225,255,60);
  ellipse(18,18,20,20);
  pop();
  
  push();
  rotate(minusVinkel10);
  fill(186,225,255,60);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(minusVinkel10);
  fill(186,225,255, 60);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(minusVinkel10);
  fill(186,225,255, 60);
  ellipse(33,33, 25, 25);
  pop();

  //minusVinkel11
  push();
  rotate(minusVinkel11);
  minusVinkel11 -=4
  fill(150,150,150,50);
  ellipse(18,18,20,20);
  pop();
  
  push();
  rotate(minusVinkel11);
  fill(150,150,150,50);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(minusVinkel11);
  fill(150,150,150,50);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(minusVinkel11);
  fill(150,150,150,50);
  ellipse(33,33, 25, 25);
  pop();

  //minusVinkler12
  push();
  rotate(minusVinkel12);
  fill(255,179,186);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(minusVinkel12);
  fill(255,179,186);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(minusVinkel12);
  minusVinkel12 -=4
  fill(255,179,186);
  ellipse(18,18,20,20);
  pop();

  push();
  rotate(minusVinkel12);
  fill(255,179,186);
  ellipse(33,33, 25, 25);
  pop();

  //minusVinusVinkel13
  push();
  rotate(minusVinkel13);
  minusVinkel13 -=4
  fill(255,223,186, 90);
  ellipse(18,18,20,20);
  pop();
  
  push();
  rotate(minusVinkel13);
  fill(255,223,186, 90);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(minusVinkel13);
  fill(255,223,186,90);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(minusVinkel13);
  fill(255,223,186,90);
  ellipse(33,33, 25, 25);
  pop();

  //minusVinkel14
  push();
  rotate(minusVinkel14);
  minusVinkel14 -=4
  fill(255,255,186,80);
  ellipse(18,18,20,20);
  pop();
  
  push();
  rotate(minusVinkel14);
  fill(255,255,186,80);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(minusVinkel14);
  fill(255,255,186, 80);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(minusVinkel14);
  fill(255,255,186, 80);
  ellipse(33,33, 25, 25);
  pop();

  //minusVinkel15
  push();
  rotate(minusVinkel15);
  minusVinkel15 -=4
  fill(186,255,201,70);
  ellipse(18,18,20,20);
  pop();
  
  push();
  rotate(minusVinkel15);
  fill(186,255,201,70);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(minusVinkel15);
  fill(186,255,201, 70);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(minusVinkel15);
  fill(186,255,201, 70);
  ellipse(33,33, 25, 25);
  pop();
 
  //minusVinkel16
  push();
  rotate(minusVinkel16);
  minusVinkel16 -=4
  fill(186,225,255,60);
  ellipse(18,18,20,20);
  pop();
  
  push();
  rotate(minusVinkel16);
  fill(186,225,255,60);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(minusVinkel16);
  fill(186,225,255, 60);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(minusVinkel16);
  fill(186,225,255, 60);
  ellipse(33,33, 25, 25);
  pop();

  //minusVinkel17
  push();
  rotate(minusVinkel17);
  minusVinkel17 -=4
  fill(150,150,150,50);
  ellipse(18,18,20,20);
  pop();
  
  push();
  rotate(minusVinkel17);
  fill(150,150,150,50);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(minusVinkel17);
  fill(150,150,150,50);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(minusVinkel17);
  fill(150,150,150,50);
  ellipse(33,33, 25, 25);
  pop();

    //minusVinkler18
    push();
    rotate(minusVinkel18);
    fill(255,179,186);
    ellipse(52,52, 30, 30);
    pop();
  
    push();
    rotate(minusVinkel18);
    fill(255,179,186);
    ellipse(74,74,35,35);
    pop();
  
    push();
    rotate(minusVinkel18);
    minusVinkel18 -=4
    fill(255,179,186);
    ellipse(18,18,20,20);
    pop();
  
    push();
    rotate(minusVinkel18);
    fill(255,179,186);
    ellipse(33,33, 25, 25);
    pop();
  
    //minusVinusVinkel19
    push();
    rotate(minusVinkel19);
    minusVinkel19 -=4
    fill(255,223,186, 90);
    ellipse(18,18,20,20);
    pop();
    
    push();
    rotate(minusVinkel19);
    fill(255,223,186, 90);
    ellipse(52,52, 30, 30);
    pop();
  
    push();
    rotate(minusVinkel19);
    fill(255,223,186,90);
    ellipse(74,74,35,35);
    pop();
  
    push();
    rotate(minusVinkel19);
    fill(255,223,186,90);
    ellipse(33,33, 25, 25);
    pop();
  
    //minusVinkel20
    push();
    rotate(minusVinkel20);
    minusVinkel20 -=4
    fill(255,255,186,80);
    ellipse(18,18,20,20);
    pop();
    
    push();
    rotate(minusVinkel20);
    fill(255,255,186,80);
    ellipse(52,52, 30, 30);
    pop();
  
    push();
    rotate(minusVinkel20);
    fill(255,255,186, 80);
    ellipse(74,74,35,35);
    pop();
  
    push();
    rotate(minusVinkel20);
    fill(255,255,186, 80);
    ellipse(33,33, 25, 25);
    pop();
  
    //minusVinkel21
    push();
    rotate(minusVinkel21);
    minusVinkel21 -=4
    fill(186,255,201,70);
    ellipse(18,18,20,20);
    pop();
    
    push();
    rotate(minusVinkel21);
    fill(186,255,201,70);
    ellipse(52,52, 30, 30);
    pop();
  
    push();
    rotate(minusVinkel21);
    fill(186,255,201, 70);
    ellipse(74,74,35,35);
    pop();
  
    push();
    rotate(minusVinkel21);
    fill(186,255,201, 70);
    ellipse(33,33, 25, 25);
    pop();
   
    //minusVinkel22
    push();
    rotate(minusVinkel22);
    minusVinkel22 -=4
    fill(186,225,255,60);
    ellipse(18,18,20,20);
    pop();
    
    push();
    rotate(minusVinkel22);
    fill(186,225,255,60);
    ellipse(52,52, 30, 30);
    pop();
  
    push();
    rotate(minusVinkel22);
    fill(186,225,255, 60);
    ellipse(74,74,35,35);
    pop();
  
    push();
    rotate(minusVinkel22);
    fill(186,225,255, 60);
    ellipse(33,33, 25, 25);
    pop();
  
    //minusVinkel23
    push();
    rotate(minusVinkel23);
    minusVinkel23 -=4
    fill(150,150,150,50);
    ellipse(18,18,20,20);
    pop();
    
    push();
    rotate(minusVinkel23);
    fill(150,150,150,50);
    ellipse(52,52, 30, 30);
    pop();
  
    push();
    rotate(minusVinkel23);
    fill(150,150,150,50);
    ellipse(74,74,35,35);
    pop();
  
    push();
    rotate(minusVinkel23);
    fill(150,150,150,50);
    ellipse(33,33, 25, 25);
    pop();

     //minusVinkler18
     push();
     rotate(minusVinkel24);
     fill(255,179,186);
     ellipse(52,52, 30, 30);
     pop();
   
     push();
     rotate(minusVinkel24);
     fill(255,179,186);
     ellipse(74,74,35,35);
     pop();
   
     push();
     rotate(minusVinkel24);
     minusVinkel24 -=4
     fill(255,179,186);
     ellipse(18,18,20,20);
     pop();
   
     push();
     rotate(minusVinkel24);
     fill(255,179,186);
     ellipse(33,33, 25, 25);
     pop();
   
     //minusVinusVinkel19
     push();
     rotate(minusVinkel25);
     minusVinkel25 -=4
     fill(255,223,186, 90);
     ellipse(18,18,20,20);
     pop();
     
     push();
     rotate(minusVinkel25);
     fill(255,223,186, 90);
     ellipse(52,52, 30, 30);
     pop();
   
     push();
     rotate(minusVinkel25);
     fill(255,223,186,90);
     ellipse(74,74,35,35);
     pop();
   
     push();
     rotate(minusVinkel25);
     fill(255,223,186,90);
     ellipse(33,33, 25, 25);
     pop();
   
     //minusVinkel20
     push();
     rotate(minusVinkel26);
     minusVinkel26 -=4
     fill(255,255,186,80);
     ellipse(18,18,20,20);
     pop();
     
     push();
     rotate(minusVinkel26);
     fill(255,255,186,80);
     ellipse(52,52, 30, 30);
     pop();
   
     push();
     rotate(minusVinkel26);
     fill(255,255,186, 80);
     ellipse(74,74,35,35);
     pop();
   
     push();
     rotate(minusVinkel26);
     fill(255,255,186, 80);
     ellipse(33,33, 25, 25);
     pop();
   
     //minusVinkel21
     push();
     rotate(minusVinkel27);
     minusVinkel27 -=4
     fill(186,255,201,70);
     ellipse(18,18,20,20);
     pop();
     
     push();
     rotate(minusVinkel27);
     fill(186,255,201,70);
     ellipse(52,52, 30, 30);
     pop();
   
     push();
     rotate(minusVinkel27);
     fill(186,255,201, 70);
     ellipse(74,74,35,35);
     pop();
   
     push();
     rotate(minusVinkel27);
     fill(186,255,201, 70);
     ellipse(33,33, 25, 25);
     pop();
    
     //minusVinkel22
     push();
     rotate(minusVinkel28);
     minusVinkel28 -=4
     fill(186,225,255,60);
     ellipse(18,18,20,20);
     pop();
     
     push();
     rotate(minusVinkel28);
     fill(186,225,255,60);
     ellipse(52,52, 30, 30);
     pop();
   
     push();
     rotate(minusVinkel28);
     fill(186,225,255, 60);
     ellipse(74,74,35,35);
     pop();
   
     push();
     rotate(minusVinkel28);
     fill(186,225,255, 60);
     ellipse(33,33, 25, 25);
     pop();
   
     //minusVinkel23
     push();
     rotate(minusVinkel29);
     minusVinkel29 -=4
     fill(150,150,150,50);
     ellipse(18,18,20,20);
     pop();
     
     push();
     rotate(minusVinkel29);
     fill(150,150,150,50);
     ellipse(52,52, 30, 30);
     pop();
   
     push();
     rotate(minusVinkel29);
     fill(150,150,150,50);
     ellipse(74,74,35,35);
     pop();
   
     push();
     rotate(minusVinkel29);
     fill(150,150,150,50);
     ellipse(33,33, 25, 25);
     pop();

      //minusVinkler18
      push();
      rotate(minusVinkel30);
      fill(255,179,186);
      ellipse(52,52, 30, 30);
      pop();
    
      push();
      rotate(minusVinkel30);
      fill(255,179,186);
      ellipse(74,74,35,35);
      pop();
    
      push();
      rotate(minusVinkel30);
      minusVinkel30 -=4
      fill(255,179,186);
      ellipse(18,18,20,20);
      pop();
    
      push();
      rotate(minusVinkel30);
      fill(255,179,186);
      ellipse(33,33, 25, 25);
      pop();
    
      //minusVinusVinkel19
      push();
      rotate(minusVinkel31);
      minusVinkel31 -=4
      fill(255,223,186, 90);
      ellipse(18,18,20,20);
      pop();
      
      push();
      rotate(minusVinkel31);
      fill(255,223,186, 90);
      ellipse(52,52, 30, 30);
      pop();
    
      push();
      rotate(minusVinkel31);
      fill(255,223,186,90);
      ellipse(74,74,35,35);
      pop();
    
      push();
      rotate(minusVinkel31);
      fill(255,223,186,90);
      ellipse(33,33, 25, 25);
      pop();
    
      //minusVinkel20
      push();
      rotate(minusVinkel32);
      minusVinkel32 -=4
      fill(255,255,186,80);
      ellipse(18,18,20,20);
      pop();
      
      push();
      rotate(minusVinkel32);
      fill(255,255,186,80);
      ellipse(52,52, 30, 30);
      pop();
    
      push();
      rotate(minusVinkel32);
      fill(255,255,186, 80);
      ellipse(74,74,35,35);
      pop();
    
      push();
      rotate(minusVinkel32);
      fill(255,255,186, 80);
      ellipse(33,33, 25, 25);
      pop();
    
      //minusVinkel21
      push();
      rotate(minusVinkel33);
      minusVinkel33 -=4
      fill(186,255,201,70);
      ellipse(18,18,20,20);
      pop();
      
      push();
      rotate(minusVinkel33);
      fill(186,255,201,70);
      ellipse(52,52, 30, 30);
      pop();
    
      push();
      rotate(minusVinkel33);
      fill(186,255,201, 70);
      ellipse(74,74,35,35);
      pop();
    
      push();
      rotate(minusVinkel33);
      fill(186,255,201, 70);
      ellipse(33,33, 25, 25);
      pop();
     
      //minusVinkel22
      push();
      rotate(minusVinkel34);
      minusVinkel34 -=4
      fill(186,225,255,60);
      ellipse(18,18,20,20);
      pop();
      
      push();
      rotate(minusVinkel34);
      fill(186,225,255,60);
      ellipse(52,52, 30, 30);
      pop();
    
      push();
      rotate(minusVinkel34);
      fill(186,225,255, 60);
      ellipse(74,74,35,35);
      pop();
    
      push();
      rotate(minusVinkel34);
      fill(186,225,255, 60);
      ellipse(33,33, 25, 25);
      pop();
  

  //vinkel5
  
  push();
  rotate(vinkel5);
  fill(100,100,100,30);
  ellipse(74,74,35,35);
  pop();
  
  push();
  rotate(vinkel5);
  fill(100,100,100,30);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(vinkel5);
  fill(100,100,100,30);
  ellipse(33,33, 25, 25);
  pop();

  push();
  rotate(vinkel5);
  vinkel5 +=5
  fill(100,100,100,30);
  ellipse(18,18,20,20);
  pop();

  //vinkel4
 
  
  push();
  rotate(vinkel4);
  fill(175,175,175,60);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(vinkel4);
  fill(175,175,175,60);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(vinkel4);
  fill(175,175,175,60);
  ellipse(33,33, 25, 25);
  pop();

  push();
  rotate(vinkel4);
  vinkel4 +=5
  fill(175,175,175,60);
  ellipse(18,18,20,20);
  pop();

  //vinkel3
 
  push();
  rotate(vinkel3);
  fill(200,200,200,70);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(vinkel3);
  fill(200,200,200,70);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(vinkel3);
  fill(200,200,200,70);
  ellipse(33,33, 25, 25);
  pop();

 push();
  rotate(vinkel3);
  vinkel3 +=5
  fill(200,200,200,70);
  ellipse(18,18,20,20);
  pop();
  
  //vinkel2
  
  push();
  rotate(vinkel2);
  fill(225,225,225,80);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(vinkel2);
  fill(225,225,225,80);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(vinkel2);
  fill(225,225,225,80);
  ellipse(33,33, 25, 25);
  pop();

  push();
  rotate(vinkel2);
  vinkel2 +=5
  fill(225,225,225,80);
  ellipse(18,18,20,20);
  pop();
  
  //vinkel1

  push();
  rotate(vinkel1);
  fill(250,250,250,90);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(vinkel1);
  fill(250,250,250,90);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(vinkel1);
  fill(250,250,250,90);
  ellipse(33,33, 25, 25);
  pop();

  push();
  rotate(vinkel1);
  vinkel1 +=5
  fill(250,250,250,90);
  ellipse(18,18,20,20);
  pop();
  
  //vinkel
  
  push();
  rotate(vinkel);
  fill(255);  
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(vinkel);
  fill(255);  
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(vinkel);
  fill(255);
  ellipse(33,33, 25, 25);
  pop();

  push();
  rotate(vinkel);
  fill(255);  
  ellipse(18,18,20,20);
  vinkel +=5
  pop();

    //vinkel11
    push();
    rotate(vinkel11);
    vinkel11 +=5
    fill(100,100,100,30);
    ellipse(18,18,20,20);
    pop();
    
    push();
    rotate(vinkel11);
    fill(100,100,100,30);
    ellipse(52,52, 30, 30);
    pop();
  
    push();
    rotate(vinkel11);
    fill(100,100,100,30);
    ellipse(74,74,35,35);
    pop();
  
    push();
    rotate(vinkel11);
    fill(100,100,100,30);
    ellipse(33,33, 25, 25);
    pop();
  
    //vinkel
    push();
    rotate(vinkel10);
    vinkel10 +=5
    fill(175,175,175,60);
    ellipse(18,18,20,20);
    pop();
    
    push();
    rotate(vinkel10);
    fill(175,175,175,60);
    ellipse(52,52, 30, 30);
    pop();
  
    push();
    rotate(vinkel10);
    fill(175,175,175,60);
    ellipse(74,74,35,35);
    pop();
  
    push();
    rotate(vinkel10);
    fill(175,175,175,60);
    ellipse(33,33, 25, 25);
    pop();
  
    //vinkel9
    push();
    rotate(vinkel9);
    vinkel9 +=5
    fill(200,200,200,70);
    ellipse(18,18,20,20);
    pop();
    
    push();
    rotate(vinkel9);
    fill(200,200,200,70);
    ellipse(52,52, 30, 30);
    pop();
  
    push();
    rotate(vinkel9);
    fill(200,200,200,70);
    ellipse(74,74,35,35);
    pop();
  
    push();
    rotate(vinkel9);
    fill(200,200,200,70);
    ellipse(33,33, 25, 25);
    pop();
  
    //vinkel2
    push();
    rotate(vinkel8);
    vinkel8 +=5
    fill(225,225,225,80);
    ellipse(18,18,20,20);
    pop();
    
    push();
    rotate(vinkel8);
    fill(225,225,225,80);
    ellipse(52,52, 30, 30);
    pop();
  
    push();
    rotate(vinkel8);
    fill(225,225,225,80);
    ellipse(74,74,35,35);
    pop();
  
    push();
    rotate(vinkel8);
    fill(225,225,225,80);
    ellipse(33,33, 25, 25);
    pop();
  
    //vinkel1
    push();
    rotate(vinkel7);
    vinkel7 +=5
    fill(250,250,250,90);
    ellipse(18,18,20,20);
    pop();
    
    push();
    rotate(vinkel7);
    fill(250,250,250,90);
    ellipse(52,52, 30, 30);
    pop();
  
    push();
    rotate(vinkel7);
    fill(250,250,250,90);
    ellipse(74,74,35,35);
    pop();
  
    push();
    rotate(vinkel7);
    fill(250,250,250,90);
    ellipse(33,33, 25, 25);
    pop();
  
    //vinkel
    push();
    rotate(vinkel6);
    fill(255);  
    ellipse(18,18,20,20);
    vinkel6 +=5
    pop();
  
    push();
    rotate(vinkel6);
    fill(255);  
    ellipse(52,52, 30, 30);
    pop();
  
    push();
    rotate(vinkel6);
    fill(255);  
    ellipse(74,74,35,35);
    pop();
  
    push();
    rotate(vinkel6);
    fill(255);
    ellipse(33,33, 25, 25);
    pop();



  //vinkel17
  push();
  rotate(vinkel17);
  vinkel17 +=5
  fill(100,100,100,30);
  ellipse(18,18,20,20);
  pop();
  
  push();
  rotate(vinkel17);
  fill(100,100,100,30);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(vinkel17);
  fill(100,100,100,30);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(vinkel17);
  fill(100,100,100,30);
  ellipse(33,33, 25, 25);
  pop();

  //vinkel16
  push();
  rotate(vinkel16);
  vinkel16 +=5
  fill(175,175,175,60);
  ellipse(18,18,20,20);
  pop();
  
  push();
  rotate(vinkel16);
  fill(175,175,175,60);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(vinkel16);
  fill(175,175,175,60);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(vinkel16);
  fill(175,175,175,60);
  ellipse(33,33, 25, 25);
  pop();

  //vinkel15
  push();
  rotate(vinkel15);
  vinkel15 +=5
  fill(200,200,200,70);
  ellipse(18,18,20,20);
  pop();
  
  push();
  rotate(vinkel15);
  fill(200,200,200,70);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(vinkel15);
  fill(200,200,200,70);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(vinkel15);
  fill(200,200,200,70);
  ellipse(33,33, 25, 25);
  pop();

  //vinkel14
  push();
  rotate(vinkel14);
  vinkel14 +=5
  fill(225,225,225,80);
  ellipse(18,18,20,20);
  pop();
  
  push();
  rotate(vinkel14);
  fill(225,225,225,80);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(vinkel14);
  fill(225,225,225,80);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(vinkel14);
  fill(225,225,225,80);
  ellipse(33,33, 25, 25);
  pop();

  //vinkel13
  push();
  rotate(vinkel13);
  vinkel13 +=5
  fill(250,250,250,90);
  ellipse(18,18,20,20);
  pop();
  
  push();
  rotate(vinkel13);
  fill(250,250,250,90);
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(vinkel13);
  fill(250,250,250,90);
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(vinkel13);
  fill(250,250,250,90);
  ellipse(33,33, 25, 25);
  pop();

  //vinkel12
  push();
  rotate(vinkel12);
  fill(255);  
  ellipse(18,18,20,20);
  vinkel12 +=5
  pop();

  push();
  rotate(vinkel12);
  fill(255);  
  ellipse(52,52, 30, 30);
  pop();

  push();
  rotate(vinkel12);
  fill(255);  
  ellipse(74,74,35,35);
  pop();

  push();
  rotate(vinkel12);
  fill(255);
  ellipse(33,33, 25, 25);
  pop();

    //vinkel23
    push();
    rotate(vinkel23);
    vinkel23 +=5
    fill(100,100,100,30);
    ellipse(18,18,20,20);
    pop();
    
    push();
    rotate(vinkel23);
    fill(100,100,100,30);
    ellipse(52,52, 30, 30);
    pop();
  
    push();
    rotate(vinkel23);
    fill(100,100,100,30);
    ellipse(74,74,35,35);
    pop();
  
    push();
    rotate(vinkel23);
    fill(100,100,100,30);
    ellipse(33,33, 25, 25);
    pop();
  
    //vinkel22
    push();
    rotate(vinkel22);
    vinkel22 +=5
    fill(175,175,175,60);
    ellipse(18,18,20,20);
    pop();
    
    push();
    rotate(vinkel22);
    fill(175,175,175,60);
    ellipse(52,52, 30, 30);
    pop();
  
    push();
    rotate(vinkel22);
    fill(175,175,175,60);
    ellipse(74,74,35,35);
    pop();
  
    push();
    rotate(vinkel22);
    fill(175,175,175,60);
    ellipse(33,33, 25, 25);
    pop();
  
    //vinkel21
    push();
    rotate(vinkel21);
    vinkel21 +=5
    fill(200,200,200,70);
    ellipse(18,18,20,20);
    pop();
    
    push();
    rotate(vinkel21);
    fill(200,200,200,70);
    ellipse(52,52, 30, 30);
    pop();
  
    push();
    rotate(vinkel21);
    fill(200,200,200,70);
    ellipse(74,74,35,35);
    pop();
  
    push();
    rotate(vinkel21);
    fill(200,200,200,70);
    ellipse(33,33, 25, 25);
    pop();
  
    //vinkel20
    push();
    rotate(vinkel20);
    vinkel20 +=5
    fill(225,225,225,80);
    ellipse(18,18,20,20);
    pop();
    
    push();
    rotate(vinkel20);
    fill(225,225,225,80);
    ellipse(52,52, 30, 30);
    pop();
  
    push();
    rotate(vinkel20);
    fill(225,225,225,80);
    ellipse(74,74,35,35);
    pop();
  
    push();
    rotate(vinkel20);
    fill(225,225,225,80);
    ellipse(33,33, 25, 25);
    pop();
  
    //vinkel19
    push();
    rotate(vinkel19);
    vinkel19 +=5
    fill(250,250,250,90);
    ellipse(18,18,20,20);
    pop();
    
    push();
    rotate(vinkel19);
    fill(250,250,250,90);
    ellipse(52,52, 30, 30);
    pop();
  
    push();
    rotate(vinkel19);
    fill(250,250,250,90);
    ellipse(74,74,35,35);
    pop();
  
    push();
    rotate(vinkel19);
    fill(250,250,250,90);
    ellipse(33,33, 25, 25);
    pop();
  
    //vinkel18
    push();
    rotate(vinkel18);
    fill(255);  
    ellipse(18,18,20,20);
    vinkel18 +=5
    pop();
  
    push();
    rotate(vinkel18);
    fill(255);  
    ellipse(52,52, 30, 30);
    pop();
  
    push();
    rotate(vinkel18);
    fill(255);  
    ellipse(74,74,35,35);
    pop();
  
    push();
    rotate(vinkel18);
    fill(255);
    ellipse(33,33, 25, 25);
    pop();
 }
