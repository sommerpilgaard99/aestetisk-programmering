Link til koden: https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix5/index.html

Link til source kode: https://gitlab.com/sommerpilgaard99/aestetisk-programmering/-/blob/main/minix5/minix5.js

![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix5/Minix5.PNG)

### Beskrivelse af mit program
Jeg har til besvarelsen af denne opgave lavet et generativt kunstværk, som ændre sig i hvert fald lidt, hver gang man kører programmet. Baggrunden er transparent, så alt udenfor mine egen givne parametre forsvinder kort efter de er generet. Selve kunsten er bygget af rectangler i variende størrelse, som også bliver spredt ud på canvas, i en random orden, men altid ud fra x-aksen, som de starter ved og slutter på.


### Koden og reglerne
1. Hvis RND < 0.5 tegnes der en rect med random højde og bredde, 20 nede på Y-aksen.
2. Hvis RND > 0.5 tegnes der en rect med random højde og bredde, 20 henne af X-aksen.
3. Farve skifte indenfor et valgt parametrer.

Jeg har brugt nogle forskellige _if_ statements til at lave koden ud fra de regler jeg selv har sat mig.

-------------------------------------------------------------
 const minRandomVærdi = random(1);

  const minRandomVærdi1 = random(50, 75);

  rectMode(CENTER);

  if(minRandomVærdi < 0.5){


    rect(x,y+minAfstand, random(20,10));
  } else{

    rect(x+minAfstand,y, random(10.20));
  }
  
-----------------------------------------------------------
Som man kan se her har sat højden og bredden til at være random indenfor de givne parametre, og ved det første _if_ har jeg sat den til at tegne min firkant med Y+minAfstand, hvilket gør at den ville blive tegnet lidt længere oppe hvis RND < 0.5.
Hvorimod at den ville blive tegnet med x+minAfstand, hvis RND > 0.5.

Man kan sagtens argumentere for at jeg kunne have gjort det en smule mere udfordrende med de regler som jeg lavede for mig selv, men jeg lavede dem ud fra nogle af de nye syntakser som vi lige havde lært om denne samme uge, så jeg følte mig ikke tryg nok med dem til at give mig selv en større udfordring. Jeg føler at fordi jeg spillede den en smule mere sikkert, blev koden ikke helt så spændende som den kunne have været, eller som jeg selv havde frostillet mig den skulle blive. Jeg havde selv tænkt, at jeg skulle prøve kræfter med at bruge noget _rotation_ da jeg føler det kunne have gjort mit kunstværk meget mere unikt, da næsten alle har brugt den samme template, ud fra det vi lavede i timen. 
Med det sagt fulgte jeg alle mine regler, og er ikke utilfreds med resultatet, jeg føler bare jeg burde have presset migselv mere end jeg gjorde. 

Jeg har også lavet et andet _if_ statement, hvor jeg har angivet 2 Y-punkter, hvor der her imellem bliver filled med et nyt sæt af random farver.

> if (x > width/2.5 && x < width/1.5){
    col = color(random(200,255));
  }
>

### Refleksioner om kontrol
Dette emne er netop spændende idet rejser nogle spørgsmål om noget overhovedet kan være random, og hvis det er random, i hvilken grad kan man så kalde det random. Jeg har lavet et program, så det ligner at det er random genereret. På en måde kan man jo kalde det random, men for at bringe det tilbage til det her om hvilken grad det er random, så har jeg jo brugt en syntakse som computeren selv kan genkende _random()_, og hvis man bare ser på det så simpelt som muligt er det jo random, men ikke helt? Computeren basere _randomness_ ud fra angivede parametre og de er jo kontrolleret af koderen, i dette tilfælde mig, så i princippet er det mig der bestemmer hvor random det kan blive. Udfra det vil jeg ikke mene at det er muligt nogensinde at kunne kode noget som er fuldstændigt random, der er altid noget kontrol involveret.
En anden ting ved det her generative kode jeg specielt er stor fan af, er det med at det aldrig bliver helt det samme.

"Once the outcome of a game is known, the game becomes meaningless. Incorporating chance into the game helps delay the
moment when the outcome will become obvious" (Montfort et al. p. 122(2012))

Dette er et citat som jeg er fuldstændigt enig med, da jeg før har gennemført spil og så aldrig spillet dem igen siden, da jeg vidste hvad der ville ske. Derfor er jeg blevet stor fan af genren af spil som hedder roquelike/roquelite, som i sig selv har et ellement af generativ kode, da hver gang man starter forfra, er der noget anderledes ved spillene. Dette er med til holde spillene spændende, da hver gang er forskellig og man ved netop ikke hvad resultatet bliver denne gang man spiller.
