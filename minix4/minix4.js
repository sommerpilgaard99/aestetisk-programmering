let knap;
let knap2;
let knap3;
let knap4;

let col;

function setup() {
  // put setup code here
createCanvas(windowWidth,windowHeight);
background(255);

col = color(0);

}


function draw() {
  // put drawing code here

  if (mouseIsPressed === true) {
    stroke(col);
    line(pmouseX,pmouseY,mouseX,mouseY)
  } 

  rectMode(CENTER);
  push();
  fill(200);
  rect(width,height/2,200, 1000);
  pop();

  push();
  fill(255,0,0);
  rect(width,49,200,100);
  pop();

  push();
  fill(0,255,0);
  rect(width,149,200,100);
  pop();

  push();
  fill(0,0,255);
  rect(width,249,200,100);
  pop();

  push();
  fill(255,255,0);
  rect(width,349,200,100);
  pop();

  push();
  fill(0,0,0);
  rect(width,449,200,100);
  pop();

  push();
  fill(255);
  rect(width,549,200,100);
  pop();

push();
  knap = createButton("Rød")
    knap.position(1179,0);
    knap.size(110,100);
    knap.style("background-color","#FF0000");
    knap.style("color","#000000");
    knap.mousePressed(farveRød);
  pop();

  push();
  knap = createButton("Grøn")
    knap.position(1179,100);
    knap.size(110,100);
    knap.style("background-color","#00FF00");
    knap.style("color","#000000");
    knap.mousePressed(farveGrøn);
  pop();

  push();
  knap = createButton("Blå")
    knap.position(1179,200);
    knap.size(110,100);
    knap.style("background-color","#0000FF");
    knap.style("color","#000000");
    knap.mousePressed(farveBlå);
  pop();  

  push();
  knap = createButton("Gul")
    knap.position(1179,300);
    knap.size(110,100);
    knap.style("background-color","#FFFF00");
    knap.style("color","#000000");
    knap.mousePressed(farveGul);
  pop();

  push();
  knap = createButton("Sort")
    knap.position(1179,400);
    knap.size(110,100);
    knap.style("background-color","#000000");
    knap.style("color","#FFFFFF");
    knap.mousePressed(farveSort);
  pop();

  push();
  knap = createButton("Viskelæder")
    knap.position(1179,500);
    knap.size(110,100);
    knap.style("background-color","#FFFFFF");
    knap.style("color","#000000");
    knap.mousePressed(visk);
  pop();

}

function farveRød(){
  col = color(255,0,0);
}

function farveGrøn(){
  col = color(0,255,0);
}

function farveBlå(){
  col = color(0,0,255);
}

function farveGul(){
  col = color(255,255,0);
}

function farveSort(){
  col = color(0);
}

function visk(){
  push();
  fill(0);
  rect(0,0,3000);
  translate(0,0);
  knap = createButton("Accepter betingelserne")
    knap.position(300,100);
    knap.size(250,200);
    knap.style("background-color","#FFFFFF");
    knap.style("color","#000000");
    knap.mousePressed();
  pop();

  push();
  translate(0,0);
  knap = createButton("X")
    knap.position(500,100);
    knap.size(50,45);
    knap.style("background-color","#FF0000");
    knap.style("color","#FFFFFF");
    knap.mousePressed(tryAgain);
  pop();
}

function tryAgain(){
  push();
  fill(0);
  rect(0,0,3000);
  translate(0,0);
  knap = createButton("Accepter betingelserne!")
    knap.position(300,100);
    knap.size(300,250);
    knap.style("background-color","#FFFFFF");
    knap.style("color","#000000");
    knap.mousePressed();
  pop();

  push();
  translate(0,0);
  knap = createButton("X")
    knap.position(550,100);
    knap.size(50,45);
    knap.style("background-color","#FF0000");
    knap.style("color","#FFFFFF");
    knap.mousePressed(tryAgain2);
  pop();
  }

  function tryAgain2(){
    push();
    fill(0);
    rect(0,0,3000);
    translate(0,0);
    knap = createButton("Accepter betingelserne!!!")
      knap.position(300,100);
      knap.size(400,350);
      knap.style("background-color","#FFFFFF");
      knap.style("color","#000000");
      knap.mousePressed();
    pop();
  
    push();
    translate(0,0);
    knap = createButton("X")
      knap.position(650,100);
      knap.size(50,45);
      knap.style("background-color","#FF0000");
      knap.style("color","#FFFFFF");
      knap.mousePressed(tryAgain3);
    pop();
    }

    function tryAgain3(){
 push();
 fill(0);
 rect(0,0,3000);
 translate(0,0);
 knap = createButton("ACCEPTER BETINGELSERNE!!!")
 knap.position(300,100);
 knap.size(600,350);
 knap.style("background-color","#FFFFFF");
 knap.style("color","#000000");
 knap.mousePressed();
 pop();

 push();
 translate(0,0);
 knap = createButton("OK")
 knap.position(575,300);
 knap.size(50,45);
 knap.style("background-color","#00FF00");
 knap.style("color","#FFFFFF");
 knap.mousePressed(tak);
 pop();
 }
    
 function tak(){
  push();
  translate(0,0);
  knap = createButton("TAK FOR ALLE DINE DATA >:D")
  knap.position(0,0);
  knap.size(1200,450);
  knap.style("background-color","#000000");
  knap.style("color","#FFFFFF");
  knap.style("font-size","40px");
  knap.mousePressed();
  pop();
 }