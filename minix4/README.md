> Programmet
(https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix4/index.html)
![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix4/HEJ.png)

#### Stuck til you accept

Med mit program her kan du gøre lige hvad du vil, du kan tegne og du kan skrive, skifte farve med knapperne ude til højre og dette kan du gøre indtil du ikke gider mere. Der sker dog noget specielt, idet man forsøger at bruge _Viskelæder_ knappen, programmet er nemlig designet til at man skal acceptere betingelserne for programmet for at kunne ændre i sine fejl. Her snyder den så også igen, for hvis man forsøger at lukke vinduet med _accepter betingelserne_ åbner den blot et nyt, større vindue med sammen beskrivelse, hvilket man kan gøre et par gange inden. Men til sidst møder man et meget stort vindue, hvor man ikke længere kan forsøge at lukke det, og man kan nu kun klikke på en grøn _OK_ knap, så man acceptere betingelserne. Dog selvom man gør dette får man ikke adgang til programmet igen, men man ender derimod med at blive mødt af en besked på skærmen _Tak for alle dine data_. Herefter er man stuck på denne skærm og man har accepteret nogle betingelser, som man ikke engang har haft muligheden for at læse, blot fordi man gerne ville tegne videre, eller slette en fejl.

#### Nye ting jeg har lært

> Link til sourcecode: https://gitlab.com/sommerpilgaard99/aestetisk-programmering/-/blob/main/minix4/minix4.js

I denne uge har jeg lært og brugt en del nye syntakser, selvom jeg har været en smule forvirret efter timerne denne uge, og måske ikke har forstået så meget som jeg gerne ville have. Jeg har brugt _createButton_ og en masse andre under syntakser til disse knapper. Jeg har lært og brugt det her med, at man kan bruge en knap til at referere til en ny _function_ og dermed kan man få en knap til at skabe en ny knap, som jeg har gjort her i min kode. Den sværeste ting jeg har forsøgt mig i, er at kunne få farven man tegner med til at skifte ved at klikke på knapperne. Jeg prøvede først på nogle andre, men ikke succesfulde metoder. Men til sidst fandt jeg ud af at jeg bare skulle lave knapper som kunne ændre på min egen variable _let col_, så selve stregens farve kunne skifte farveværdi.
Med denne løsning fik jeg dog ikke helt det resultat som jeg havde ønsket mig, selvom jeg til sidst blev meget tilfreds med løsningne alligevel. Det jeg gerneville have gjort, var at jeg ville have det sådan at når man tryggede på "hviskelæder" knappen, skulle skærmen blive helt sort, og der skulle være kommet "accepter betingelserne op i midten af skærmen. Nu tænker du sikkertat det er det som sker, men jeg vidste ikke hvordan man fjernede alt fra før da jeg lavede koden, så hver gang du trykker på "X", fjerner du intet af det bagved, det er bare større knapper, som kommer ind foran det andet som er på skærmen.

#### Articulation
Som sagt tidligere i beskrivelsen af mit program, har jeg jo lavet denne metode til at få alle, som gerne vil benytte programmet til dets fulde, til at acceptere betingelserne uden at kunne læse hvad de er. Dette er gjort for ligesom at kunne vise, at de fleste, uden virkelig at tænke over det, bare acceptere de fleste ting, hvis de kommer i vejen for deres benyttelse af et program, selvom dette kunne betyde at de "frivilligt" giver adgang til meget af deres data.

"Capture all" handler om hvordan vi lever i et samfund, hvori alt kan blive set som data og det er noget vi lever i, de fleste tilfælde kan vores date blive brugt uanset om vi vil det eller ej. Man kan beskrive det lidt ved at bruge begrebet 'datafication' som Mejias, Ulises & Couldry snakker om i deres tekst (2019):

"Rather, datafication is a contemporary phenomenon which refers to the quantification of human life through digital information, very often for economic value."

Man kan godt være undskyldt for at tænke "Jamen nu hvor jeg ved det, kan jeg jo undgå det bedre end før" men dette er desværre svære ind man tror. Det som bliver sagt med dette citat fra bogen, er at vores eksistens, bare det at vi findes, gør at der kan findes noget af vores data i et eller andet system, og det bliver opdateret samtid med at vi lever. Så vi undgår aldrig at give data i en hvis forstand, dette er igen noget af det jeg prøver på at sige med mit program, du har ikke et valg, de skal nok få fat i din data, det er blot et spørgsmål om tid.


