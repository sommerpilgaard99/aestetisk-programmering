Link til program: (https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix6/index.html)

Link til Kode: (https://gitlab.com/sommerpilgaard99/aestetisk-programmering/-/blob/main/minix6/game.js)

![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix6/Minix6.jpg)

### Beskrivelse af mit program
I mit program har jeg designet en lille "turret" som er ens _player_ i dette spil, men styre med pilene højre og venstre, og man skyder med op pilen. Man skal skyde bobblerne og når man gør dette, deler de sig til mindre bobler man så også skal skyde. Jeg nåede aldrig at få inført en slutning eller et scoresystem til spillet, men det var planen, og det nåede jeg ikke da det var meget omfattende bare at få koden til at ville, som jeg gerne ville.

### Koden og hvad jeg har gjort
Jeg har forsøgt mig i det vi lærte med _Classes_ som vi lærte om i timerne, og det føler jeg også jeg har fået godt med her, jeg har lavet hele 3 _Classes_ til mine forskellige objekter i spillet, og det har gjort min kode meget mindre end den ville have været uden, selvom det til tider var svært at have med at gøre.

![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix6/CLASSES.jpg)

Det første jeg gjorde var at lave mine bobler og det var her jeg lærte noget vigtigt som hjalp mig meget. I klassen lærte vi at vi først skulle give den nogle en _constructor_ hvor vi skulle skrive nogle parametre ind under, men da det senere gav problemer med det jeg forsøgte mig i at gøre, fandt jeg ud af at jeg var nød til at lave alle mine _Classes_ lidt anderledes, hvilket jeg gjorde ved at gøre mine _Classes_ til functioner i stedet. Dette gjorde at jeg kunne komme af med _constructor_ så jeg frit kunne ændre på _pos_ ud fra de _vectorer_ jeg skulle lave senere.
`function Bubble(pos,r){`

`function Gun(spos, angle) {`

`function Player()` 

Den anden vigtige ting at nævne er at jeg brugte en række af _if statement_ til at fortælle computeren om hvornår "skudene" rammer "boblerne".

![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix6/Collision.jpg)

Som man kan se her har jeg brugt _for()_ til at skabe flere "guns" og "bubbles", hvilket jeg kan da jeg har gjort dem begge til _Arrays_ længere oppe. I det første af disse _if statements_ har jeg sagt at hvis skudet flyver ud _offscreen()_ skal skudet _splice()_ altså slettes helt. Så i mit _else()_ har jeg sagt at hvis skudet rammer en boble, skal den dele boblen så længe dens radius er mere end 40, hvis den er mindre end 40 forsvinder boblen, og skudet. I starten var det svært for mig at få den til at registrerer at der skulle ske noget andeledes ved de 2 forskellige radiuser, og spillet blev bare ved med at crashe hvis man ramte en bubble. Det var først da jeg lærte hvad _break_ var at jeg kunne få det hele til at virke.

Der er meget mere som kan siges om min kode, men ville skulle skrive 4 sider for at gå igennem det hele. :D

### Reflektioner
Jeg fik idéen til mit spil fra et meget gammelt arkadespil, som jeg selv har prøvet mange gange, da min far var stor fan af det, og endda endnu mere da man kunne få det på mobilen, spillet jeg snakker om er selvfølgelig SpaceInvaders. Selvom mit spil ikke direkte ligner, og nogle af spillet core mechanics er anderledes, mener jeg at inspirationen er tydeligt, "skyd fra bunden af skærmen og ram objekterne i luften". Imodsætning til SpaceInvaders har jeg også undladet at lave en grafik som var lignende, da jeg heller  ikke vidste hvordan jeg skulle have båret mig ad med at lave den til "pixel art".

Ser man blot på mit spil, er den næsten umuligt at regne ud hvad man kan, skal, vil, eller må, hvilket skyldes at jeg ikke har givet en eneste forkling til noget, hverken hvordan man spiller, eller hvad man skal gøre. Dette er på grund af jeg havde så svært ved at lave en så kompliceret kode, at jeg endte med at løbe tør for tid, så jeg fik hverken lavet et scoresystem, en måde at tabe, eller en guide. Det kan gøre det svært for spillerne at vide hvad objekterne så kan, med mindre de har læst noget af denne _readMe_. Man ville måske kunne argumentere bedre for at brugerene ville kunne regne ud hvad de skulle kunne, havde min _player_ lignet en våbentårn, ellerhvis bubblerne havde lignet noget mere truende, men som det ser ud nu, er der ikke meget chance for at den bliver regnet ud uden forklaring.

Jeg har altid haft en kæmpe interesse for spil, da jeg er vokset op iet hjem, hvor det har fyldt en meget stor del af når vi skulle have det sjovt. Til familieaftner spillede vi MarioParty og MarioKart. Så endelig selv at kunne lave et spil og læse om hvad et spil i sin essens er, var meget spændende for mig personligt. Jeg synes det var interessant at lære om hvad der skulle til for at kunne  kalde det et spil, og hvordan man skulle programmere objektsorienteret. Jeg har ikke selv opfyldt disse krav så godt som jeg kunnet have håbet på, men dette er bestemt ikke det sidste spil jeg vil prøve at lave.
