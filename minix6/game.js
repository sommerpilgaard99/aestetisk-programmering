let bubble = [];
let player;
let gun = [];
antal = 5;
function setup() {
  // put setup code here
  createCanvas(1200, 500);
  background(255);
  for (let i = 0; i < antal; i++) {
    bubble.push(new Bubble());
  }

  player = new Player();
  frameRate(30);
}

function draw() {
  // put drawing code here
  background(0);
  //bubble.popped();
  if(random(1)<0.005) {
    bubble.push(new Bubble());
  }

  for (var i = 0; i < bubble.length; i++) {
  bubble[i].edges();
  bubble[i].show();
  bubble[i].update();
  bubble[i].move();
  }


  for (let i = gun.length - 1; i >= 0; i--) {
    gun[i].render();
    gun[i].update();
    if (gun[i].offscreen()) {
      gun.splice(i, 1);
    } else {
      for (let j = bubble.length - 1; j >= 0; j--) {
        if (gun[i].hits(bubble[j])) {
          if (bubble[j].r > 40){
            let newBubbles = bubble[j].breakup();
            bubble = bubble.concat(newBubbles);
          }
          bubble.splice(j, 1);
          gun.splice(i, 1);
          break;
        }
      }
    }
  }
  player.show();
  player.turn();
  player.edges();

}


function keyReleased() {
  player.setRotation(0);
  
}

function keyPressed() {
  if (keyCode == UP_ARROW) {
    gun.push(new Gun(player.pos, player.heading));
  }
  else if (keyCode == LEFT_ARROW) {
    player.setRotation(-0.1);
  }
  else if (keyCode == RIGHT_ARROW) {
    player.setRotation(0.1);
  }

}
