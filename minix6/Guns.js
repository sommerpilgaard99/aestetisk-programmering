function Gun(spos, angle) {
    this.pos = createVector(spos.x, spos.y);
    this.vel = p5.Vector.fromAngle(angle);
    this.vel.mult(10);

    this.update = function () {
        this.pos.add(this.vel);
    };

    this.render = function () {
        push();
        stroke(random(190,255), random(254,255), random(190,255));
        strokeWeight(5);
        point(this.pos.x, this.pos.y);
        pop();
    };

    this.hits = function (bubble) {
        let d = dist(this.pos.x, this.pos.y, bubble.pos.x, bubble.pos.y);
        if (d < bubble.r){
            return true;
        } else {
            return false;
        }
    };

    this.offscreen = function(){
        if (this.pos.x > width || this.pos.x < 0) {
            return true;
        }
        if (this.pos.y > height || this.pos.y < 0) {
            return true;
        }
        return false;
    };
}