function Bubble(pos,r){
  if (pos) {
    this.pos = pos.copy();
  } else {
    this.pos = createVector(random(width), random(height));
  }
  if (r) {
    this.r = r * 0.6;
  } else {
    this.r = random(50, 100);
  }


this.move = function(){
  this.pos.x = this.pos.x + random(5);
  this.pos.y = this.pos.y + random(5);
  this.pos.x = this.pos.x - random(5);
  this.pos.y = this.pos.y - random(5);
 
}

this.vel = p5.Vector.random2D();
  this.total = floor(random(5, 15));
  this.offset = [];
  for (var i = 0; i < this.total; i++) {
    this.offset[i] = random(-this.r * 0.5, this.r * 0.5);
  }


this.update = function() {
  this.pos.add(this.vel);
};


this.show = function(){
 stroke(255);
 strokeWeight(4);
 noFill();
 ellipse(this.pos.x, this.pos.y, this.r);
}

this.breakup = function() {
  var newB = [];
  newB[0] = new Bubble(this.pos, this.r);
  newB[1] = new Bubble(this.pos, this.r);
  return newB;
};



this.edges = function() {
  if (this.pos.x > width + this.r) {
    this.pos.x = -this.r;
  } else if (this.pos.x < -this.r) {
    this.pos.x = width + this.r;
  }
  if (this.pos.y > height + this.r) {
    this.pos.y = -this.r;
  } else if (this.pos.y < -this.r) {
    this.pos.y = height + this.r;
  }
};
}


/*popped(){
  

}
*/

