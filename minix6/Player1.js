function Player() {
  this.pos = createVector(width/ 2, height);
  this.r = 0;
  this.heading = 4.7;
  this.rotation = 0;
  this.vel = createVector(0, 0);
  

  this.show = function () {
    push();
    translate(this.pos.x, this.pos.y);
    rotate(this.heading + PI / 2);
    fill(50, 50, 50);
    ellipse(this.r, this.r, 50,100);
    pop();
  }

  this.edges = function () {
    if (this.pos.x > width + this.r) {
      this.pos.x = -this.r;
    } else if (this.pos.x < -this.r) {
      this.pos.x = width + this.r;
    }
    if (this.pos.y > height + this.r) {
      this.pos.y = -this.r;
    } else if (this.pos.y < -this.r) {
      this.pos.y = height + this.r;
    }
  };

  this.setRotation = function (a) {
    this.rotation = a;
  };

  this.turn = function () {
    this.heading += this.rotation;
  };


}
