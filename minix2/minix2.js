function setup() {
  // put setup code here
 createCanvas(1000,500);
 background(0,0,255);
 angleMode(DEGREES);
}

function draw() {
  // put drawing code here
  push();
  fill(218,165,32);
  translate(185,310);
  rotate(30);
  rect(0,0, 50,115);
  pop();

  push();
  fill(218,165,32);
  translate(230,330);
  rotate(335);
  rect(0,0, 50,115);
  pop();

  push();
  fill(200);
  rect(100, 50, 265, 315);
  pop();

  push();
  noStroke();
  rect(100, 50, 250, 300);
  pop();
  
 
  
  push();
  if(mouseX > 100 && mouseX < 350 && mouseY > 50 && mouseY < 350){
   strokeWeight(3.0);
    fill(255,215,0);
   ellipse(225, 200, 150, 150);
   fill(0); 
   noStroke();
   ellipse(200, 175, 25,25);
   ellipse(250,175,25,25);
   fill(255);
   circle(195,169,10);
   circle(245,169,10);
   fill(0);
   arc(225, 215, 80, 70, 0, 180);

  } else{
    fill(255);
    noStroke();
    ellipse(225,200,100,100);
  }
  pop();

  push();
  fill(218,165,32);
  translate(350,250);
  rotate(135);
  ellipse(0,0, 20,200);
  pop();

  push();
  fill(0);
  translate(310,200);
  rotate(135);
  rect(0,0, 16,40);
  pop();
  
  // Anden Emoji
  push();
  strokeWeight(3.0);
    fill(255,215,0);
   ellipse(725, 200, 200, 200);
   fill(0); 
   noStroke();
   ellipse(690, 175, 40,40);
   fill(255);
   circle(680,165,15);
   fill(0);
   arc(725, 230, 90, 90, 0, 180);
   pop();

   push();
   strokeWeight(10);
   line(750, 165, 770, 185);
   line(750, 185, 770, 165);
   pop();
}
