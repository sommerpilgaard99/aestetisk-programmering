## MiniX 2
Program: https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix2/index.html

Koden: https://gitlab.com/sommerpilgaard99/aestetisk-programmering/-/blob/main/minix2/minix2.js

#### Canvas
I min MiniX 2 har jeg, ligesom alle andre, skulle lave to nye emojis.
Den første af disse to jeg har lavet kan i se på billedet herunder:
>![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix2/CANVAS.png)

Som man kan se her, har jeg skabt et canvas på min baggrund, med nogle små ben, en lille pensel og et smilende ansigt i midten. 
Da jeg først startede ud med at lave lige netop den her af mine 2 emojis, havde jeg allerede en idé om at jeg gerne ville lave det her lille lærred, men jeg tænkte længe over hvad jeg skulle gøre med det, om jeg bare skulle have det tomt eller hvad. 
Jeg  kom derfor på idéen om at man selv skulle kunne tegne på lærredet med sin mus, men da jeg forsøgte mig i at gøre så det  var muligt, endte jeg med at give op, da jeg ikke vidste hvad jeg skulle gøre.
Jeg endte derfor ud med at lave dette ansigt, som man kun kan se hvis man kører musen hen over lærredet.

Lige præcist det gjorde jeg ved at gøre brug af noget af det som vi fik vist i timerne, det her med _if_ statements vi lærte.
![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix2/IF_canvas.png)

Jeg brugte det vi havde lært med at få noget til at ændre sig når musen ramte mellem et bestemt x & y punkt, i dette tilfælde var det inden for lærredet.

Jeg gav mig også i kast med noget nyt som jeg ikke havde lært noget om inden, som var at rotere nogle af de figure jeg havde med, netop benene til mit lærred og penslen.
> ![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix2/Pensel_canvas.png)

Her på billedet kan man se hvordan jeg gjort ved ellipsen til min pensel, hvordan jeg har startet med at bruge _translate_ til at sætte et punkt som den skulle rotere om. Så har jeg også brugt grader i min rotation i stedet for _radians_ som vi har hørt lidt om, og dette er gjort ved at skrive en kode der heder _angleMode(DEGREES)_ under mit setup, som gjorde at radians blev til grader.

Jeg lavede denne emoji som værende en måde, at sige noget om dette med at der  hele  tiden bliver skabt nye emoji's til at dække for alle forskellige grupper af mennesker. Det her med at folk altid har en ny ting at brokke sig over, når det kommer til at emojis skal være inkluderende for alle. Med min canvas emoji her, skulle det jo havde været muligt at man skulle kunne tegne sin egen emoji på den. Grunden var at jeg tænkte dette som værende en måde hvorpå at alle kunne være tilfredse, hvis man kan tegne hvad man selv syntes den skulle ligne, hvem ville så være udenfor? Selvfølgelig ville dette aldrig kunne involvere alle, men det dækker da over, hvad jeg ville kalde største delen af alle som benytter emojis.

#### Enøjet emoji

Til min anden emoji har jeg skabt en lidt mere personlig emoji, den ligner egentligt meget en hver anden smiley, både med farve og ansigt, der er dog en forskel, den har kun et virkende øje.

![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix2/En%C3%B8jet_emoji.png)

For at vise det her med, at det ene øje ikke virker, har jeg givet den et lille _x_ hvor det andet øje burde have været. Jeg har egentligt ikke arbejdet med nogle nye syntakser med denne her emoji, da jeg brugte mere af det nye i den første. Så denne emoji er meget mere simpel og er skabt med forskellige figurer og streger. Eneste større problem jeg løb ind i, i min process var igen de her streger, som også vagte mig problemer i min MiniX1. Dette er fordi den har både _x1, x2, y1, y2_ punkter, og jeg har svært ved at pinpointe hvor jeg vil have dem.

Denne emoji har været mere personlig for mig, da jeg selv kun har et øje som virker ordenligt. Jeg har dog aldrig haft et problem med, at der ikke har været en smiley som har repræsenteret mit problem, men det var det som virkede som inspiration til den her emoji.
> ![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix2/En%C3%B8jet_kode.png)
Her er koden til min anden emoji.
