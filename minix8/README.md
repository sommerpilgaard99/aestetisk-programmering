# Min flowchart
Jeg har valgt at lave en flowchart af min minix4, som er en af de mere avancerede minix'er jeg har lavet. Jeg lavede mit flowchart ud fra kun nogle af de valg man har, men også med flere forskellige måder at nå slutningen.

![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix8/Flowchart_4.jpg)

Jeg har valgt at gå med et forholdsvis simpelt flowchart, selvom der nogen steder nok er områder jeg kan gå mere i dybden med. Men forsøget med dette flowchart var at gøre det nemt og forståeligt. Derfor er det endt ud på denne måde. Jeg synes flowcharts er nemme nok, så længe man forsøger at holde dem simple, så snart man skal have alle detaljer med, virker det mere svært end det er nyttigt.


# Gruppe flowchart

## Idé 1 - Pacman
Vi vil gerne lave en form for pacman. 
Man skal styre en person/figur rundt i en form for labyrint. Her handler det om at skulle samle nogle mønter, mens man skal skyde fjenden for at holde sig i live. 
Man vinder når man har samlet alle mønterne, og man taber fjenden, rammer og dræber en. 

Så vi ville lave en class til:
- Pacman 
- Fjenden 
- skuddene

Og det hele ville blive sat sammen inde i selve spillets kode. 
Man starter med at komme ind på en side hvor man får nogle oplysninger om spillet, altså fx at man skal bruge piletasterne til at styre figuren og mellemrumstasten til at skyde fjenden. Her på denne startside skal man også trykke på start for at komme i gang med spillet. 
Når  spillet går i gang, kommer man ind på en side hvor man er i en form for labyrint (man kan se det hele oppefra, ligesom i det pacman man kender) og herfra gælder det så bare om at fuldføre spillet, altså samle alle mønter mens man undgå og/eller skyder fjenden. 

![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix8/Flowchart_1.jpg)

## Idé 2 - Emojis - Personlige emojis

En tilpasselig emoji til brug på sociale medier og/eller andre steder. 
Det skal være emojis der har en følelse, såsom glad, sur, ked, overrasket ovs. altså følelser vi alle kender, og så skal man kunne tilpasse hver enkelt emoji. 

Vi vil gerne lave en/flere emojis, som man selv kan styre udseendet på, det skal både være i forhold til formen, farve, størrelse og små mere personlige detaljer, såsom farver på øjne, til -og fravalg af hår, øjne, øjenvipper osv.  Det eneste, der ligesom skal være statisk i emojien, er den følelse, den skal udtrykke. 

Så du starter med at vælge din følelse fx. glad. Derefter vælger du så, hvordan smilet skal se ud, altså hvor bredt det skal være, viser det tænder osv. Dernæst vælger du hvilken form emojien skal være, om den skal være rund, firkantet, trekantet, ligne en hovedform eller andet. Efter det vælger du hvilken farve man gerne vil have den til at være, og her kan man vælge lige den farve man har lyst til, alle farver er tilgængelige. Så vælger du hvor stor din emojis skal være, det er mest hvis du sender den i en besked, så man kan vælge hvor meget den skal fylde. 
Til sidst kan du så komme med personlige detaljer, her kommer man selv ind med ideer til hvad man gerne vil lave. Man kan fx skrive at man gerne vil have den, skal have langt lyst hår, og så sætter den det på, eller man kan sige at man gerne vil have den, skal have en skæv næse eller hvad man ellers gerne vil have lavet på den. 

Når man arbejder med at skulle lave en tilpasselig emoji, skal man også passe på, hvad man kan lave om på, da hvis man kan lave om på udseendet, er det  også muligt at bruge det til at gøre grin med andre kulturer og racer, da der ikke er noget som stopper en fra at lave en grim emoji, som har nogle ligheder til racer og kulturer.
Men på samme tid vil vi også mene at det kan have en positiv effekt, da denne måde at kunne tilpasse sine emojis, gør at det bliver meget mere personlig emoji, som kan sige lige hvad du gerne vil have.

![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix8/Flowchart_2.jpg)
![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix8/Flowchart_3.jpg)



## What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?
Uanset hvilket fag du arbejder indenfor, så er det svært at 'oversætte' fagsprog til normalt sprog som gør det muligt for alle typer af mennesker at forstå det. Et eksempel på det er flowcharts, som bruges til at give et overblik over en process eller i vores tilfælde, en kode. Et flowchart er i sig selv komplex og bruger f.eks. bestemte former til at forklare processer og ligenende. Det kan være svært at forklare kode med ord til en person der har ingen erfaring med kode. Derfor kan der ofte opstå problemer med kommunikationen eller forståelsen af kodens opbygning. Dog kan man formulere sig ud af det meste, hvilket vi benyttede os af i vores flowcharts. Vi brugte formerne i vores flowcharts til at give os selv et overblik over kodningen (f.eks. en bestemt form til if-statemts og lignende), hvor teksten så kan bruges til at gøre det 'nemt' og forståeligt for alle. Man skal også tænke på, at det produkt man er ved at lave er tænkt til at blive brugt af helt almindelige mennesker. Derfor er det også vigtig at man ved at alt der er med i ens kode kan blive gjort forståeligt for brugeren. Dette er også noget flowcharten kan hjælpe med. 

## What are the technical challenges facing the two ideas and how are you going to address these?
I begge idéer er der nogle klare udfordringer, som vil være særdeles tidskrævende at løse. Den første idé har en del udfordringer, i og med vi ville skulle lave et helt spil. Men det vil kunne gøres, så længe tiden er til at skrive selve koden. Det hjælper at vi ville have noget materiale at gå ud fra, fra de Pacman-spil, der allerede eksisterer.
I begge vores idéer har vi måske sat baren en smule højt, fordi vi ikke skulle lave noget endeligt produkt i kode. Og hvis vi gik videre med enten den ene eller den anden, er der sikkert nogle ting vi ville blive nødt til at gå på kompromis med. Men sådan er det jo i en designproces.


## In which ways are the individual and the group flowcharts you produced useful?
Flowcharts kan være meget brugbare for at skabe et overblik inden man kaster sig ud i at lave et projekt. Ofte synes mange det er irriterende eller spild af tid at lave et flowchart og ikke bare gå i gang med processen med det samme. Men flowcharts kan blandt andet blive brugt til at finde fejl inden man har spildt til tid og ressourcer på projektet. Man kan også benytte flowcharts til at gøre det mulig for andre at reflektere og give ideer/rettelser til ens produkt før man er startet. Derfor gør det hele processen mere effektiv og forhåbentlig med mindre fejl.

I vores individuelle flowchart skulle vi kigge tilbage på en tidligere kode og lave et flowchart ud fra den. Dette gør det muligt -som nævnt tidligere- for alle at forstå ens produkt. Men på samme side kan man også selv se om der er elementer af ens produkt der ikke giver mening i det store billede. Det er en måde at se løse ender på og sørge for at ens kode og metode er som den skal være, samt måske se om der er overflødige elementer med som ikke behøver at være i koden.
