let regnbue = []
let antalRegnbue = 6
let vinkel = []
let antalVinkel = 4

function setup() {
  //create a drawing canvas
  createCanvas(windowWidth, windowHeight);
  background(255);
  angleMode(DEGREES);
  for (let i = 0; i < antalRegnbue; i++) {
    regnbue.push(new Regnbue());
  }
  for (let i = 0; i < antalVinkel; i++) {
    vinkel.push(new Vinkel());
  }
 }
 
 function draw() {
  translate(width/2,height/2);
  background(0,0,0, 10);

  noStroke();
  spawnRegnbue();
  spawnVinkel();
 }

 function spawnRegnbue(){
 for (let i = 0; i < regnbue.length; i++) {
  regnbue[i].show();
  regnbue[i].move();
  }
  }

  function spawnVinkel(){
    for (let i = 0; i < vinkel.length; i++){
    vinkel[i].show();
    vinkel[i].move();
  }
}