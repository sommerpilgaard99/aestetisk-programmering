class Vinkel {
    constructor() {
        this.x = 74;
        this.y = 74;
        this.vinkel = 0;
        this.col = color(255);
        this.col1 = color(250, 250, 250, 90);
        this.col2 = color(225, 225, 225, 60);
        this.col3 = color(200, 200, 200, 50);
        this.col4 = color(175, 175, 175, 40);
        this.col5 = color(100, 100, 100, 30);
    }

    show() {
        push();
        fill(this.col);
        rotate(this.vinkel);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col1);
        rotate(this.vinkel - 10);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col2);
        rotate(this.vinkel - 20);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col3);
        rotate(this.vinkel - 30);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col4);
        rotate(this.vinkel - 40);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col5);
        rotate(this.vinkel - 50);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        /////////////////////////////////////////////////////////////////////////

        push();
        fill(this.col);
        rotate(this.vinkel - 90);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col1);
        rotate(this.vinkel - 100);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col2);
        rotate(this.vinkel - 110);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col3);
        rotate(this.vinkel - 120);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col4);
        rotate(this.vinkel - 130);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col5);
        rotate(this.vinkel - 140);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        ////////////////////////////////////////////////////////////////////////////////////

        push();
        fill(this.col);
        rotate(this.vinkel - 180);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col1);
        rotate(this.vinkel - 190);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col2);
        rotate(this.vinkel - 200);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col3);
        rotate(this.vinkel - 210);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col4);
        rotate(this.vinkel - 220);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col5);
        rotate(this.vinkel - 230);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        ///////////////////////////////////////////////////////////////////////////////////////

        push();
        fill(this.col);
        rotate(this.vinkel - 270);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col1);
        rotate(this.vinkel - 280);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col2);
        rotate(this.vinkel - 290);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col3);
        rotate(this.vinkel - 300);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col4);
        rotate(this.vinkel - 310);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col5);
        rotate(this.vinkel - 320);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();
    }

    move() {
        this.vinkel += 5
    }
}

