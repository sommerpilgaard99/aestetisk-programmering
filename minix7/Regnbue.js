class Regnbue {
    constructor() {
        this.x = 74;
        this.y = 74;
        this.minusVinkel = 0;
        this.col = color(255, 179, 186);
        this.col1 = color(255, 223, 186, 90);
        this.col2 = color(255, 255, 186, 80);
        this.col3 = color(186, 255, 201, 70);
        this.col4 = color(186, 225, 255, 60);
        this.col5 = color(150, 150, 150, 50);
    }

    show() {
        push();
        fill(this.col);
        rotate(this.minusVinkel);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col1);
        rotate(this.minusVinkel + 10);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col2);
        rotate(this.minusVinkel + 20);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col3);
        rotate(this.minusVinkel + 30);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col4);
        rotate(this.minusVinkel + 40);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col5);
        rotate(this.minusVinkel + 50);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        ////////////////////////////////////////////////////////

        push();
        fill(this.col);
        rotate(this.minusVinkel + 60);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col1);
        rotate(this.minusVinkel + 70);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col2);
        rotate(this.minusVinkel + 80);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col3);
        rotate(this.minusVinkel + 90);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col4);
        rotate(this.minusVinkel + 100);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col5);
        rotate(this.minusVinkel + 110);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        ////////////////////////////////////////////////////////

        push();
        fill(this.col);
        rotate(this.minusVinkel + 120);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col1);
        rotate(this.minusVinkel + 130);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col2);
        rotate(this.minusVinkel + 140);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col3);
        rotate(this.minusVinkel + 150);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col4);
        rotate(this.minusVinkel + 160);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        push();
        fill(this.col5);
        rotate(this.minusVinkel + 170);
        ellipse(this.x, this.y, 35, 35);
        ellipse(this.x - 22, this.y - 22, 30, 30);
        ellipse(this.x - 41, this.y - 41, 25, 25);
        ellipse(this.x - 56, this.y - 56, 20, 20);
        pop();

        ////////////////////////////////////////////////////////////

        push();
        fill(this.col);
        rotate(this.minusVinkel + 180);
        ellipse(this.x, this.y, 35,35);
        ellipse(this.x - 22, this.y - 22, 30,30);
        ellipse(this.x - 41, this.y - 41, 25,25);
        ellipse(this.x - 56, this.y - 56, 20,20);
        pop();

        push();
        fill(this.col1);
        rotate(this.minusVinkel + 190);
        ellipse(this.x, this.y, 35,35);
        ellipse(this.x - 22, this.y - 22, 30,30);
        ellipse(this.x - 41, this.y - 41, 25,25);
        ellipse(this.x - 56, this.y - 56, 20,20);
        pop();

        push();
        fill(this.col2);
        rotate(this.minusVinkel + 200);
        ellipse(this.x, this.y, 35,35);
        ellipse(this.x - 22, this.y - 22, 30,30);
        ellipse(this.x - 41, this.y - 41, 25,25);
        ellipse(this.x - 56, this.y - 56, 20,20);
        pop();

        push();
        fill(this.col3);
        rotate(this.minusVinkel + 210);
        ellipse(this.x, this.y, 35,35);
        ellipse(this.x - 22, this.y - 22, 30,30);
        ellipse(this.x - 41, this.y - 41, 25,25);
        ellipse(this.x - 56, this.y - 56, 20,20);
        pop();

        push();
        fill(this.col4);
        rotate(this.minusVinkel + 220);
        ellipse(this.x, this.y, 35,35);
        ellipse(this.x - 22, this.y - 22, 30,30);
        ellipse(this.x - 41, this.y - 41, 25,25);
        ellipse(this.x - 56, this.y - 56, 20,20);
        pop();

        push();
        fill(this.col5);
        rotate(this.minusVinkel + 230);
        ellipse(this.x, this.y, 35,35);
        ellipse(this.x - 22, this.y - 22, 30,30);
        ellipse(this.x - 41, this.y - 41, 25,25);
        ellipse(this.x - 56, this.y - 56, 20,20);
        pop();

        ////////////////////////////////////////////////////////////////////

        push();
        fill(this.col);
        rotate(this.minusVinkel + 240);
        ellipse(this.x, this.y, 35,35);
        ellipse(this.x - 22, this.y - 22, 30,30);
        ellipse(this.x - 41, this.y - 41, 25,25);
        ellipse(this.x - 56, this.y - 56, 20,20);
        pop();

        push();
        fill(this.col1);
        rotate(this.minusVinkel + 250);
        ellipse(this.x, this.y, 35,35);
        ellipse(this.x - 22, this.y - 22, 30,30);
        ellipse(this.x - 41, this.y - 41, 25,25);
        ellipse(this.x - 56, this.y - 56, 20,20);
        pop();

        push();
        fill(this.col2);
        rotate(this.minusVinkel + 260);
        ellipse(this.x, this.y, 35,35);
        ellipse(this.x - 22, this.y - 22, 30,30);
        ellipse(this.x - 41, this.y - 41, 25,25);
        ellipse(this.x - 56, this.y - 56, 20,20);
        pop();

        push();
        fill(this.col3);
        rotate(this.minusVinkel + 270);
        ellipse(this.x, this.y, 35,35);
        ellipse(this.x - 22, this.y - 22, 30,30);
        ellipse(this.x - 41, this.y - 41, 25,25);
        ellipse(this.x - 56, this.y - 56, 20,20);
        pop();

        push();
        fill(this.col4);
        rotate(this.minusVinkel + 280);
        ellipse(this.x, this.y, 35,35);
        ellipse(this.x - 22, this.y - 22, 30,30);
        ellipse(this.x - 41, this.y - 41, 25,25);
        ellipse(this.x - 56, this.y - 56, 20,20);
        pop();

        push();
        fill(this.col5);
        rotate(this.minusVinkel + 290);
        ellipse(this.x, this.y, 35,35);
        ellipse(this.x - 22, this.y - 22, 30,30);
        ellipse(this.x - 41, this.y - 41, 25,25);
        ellipse(this.x - 56, this.y - 56, 20,20);
        pop();

        /////////////////////////////////////////////////////////////////////////

        push();
        fill(this.col);
        rotate(this.minusVinkel + 300);
        ellipse(this.x, this.y, 35,35);
        ellipse(this.x - 22, this.y - 22, 30,30);
        ellipse(this.x - 41, this.y - 41, 25,25);
        ellipse(this.x - 56, this.y - 56, 20,20);
        pop();

        push();
        fill(this.col1);
        rotate(this.minusVinkel + 310);
        ellipse(this.x, this.y, 35,35);
        ellipse(this.x - 22, this.y - 22, 30,30);
        ellipse(this.x - 41, this.y - 41, 25,25);
        ellipse(this.x - 56, this.y - 56, 20,20);
        pop();

        push();
        fill(this.col2);
        rotate(this.minusVinkel + 320);
        ellipse(this.x, this.y, 35,35);
        ellipse(this.x - 22, this.y - 22, 30,30);
        ellipse(this.x - 41, this.y - 41, 25,25);
        ellipse(this.x - 56, this.y - 56, 20,20);
        pop();

        push();
        fill(this.col3);
        rotate(this.minusVinkel + 330);
        ellipse(this.x, this.y, 35,35);
        ellipse(this.x - 22, this.y - 22, 30,30);
        ellipse(this.x - 41, this.y - 41, 25,25);
        ellipse(this.x - 56, this.y - 56, 20,20);
        pop();

        push();
        fill(this.col4);
        rotate(this.minusVinkel + 340);
        ellipse(this.x, this.y, 35,35);
        ellipse(this.x - 22, this.y - 22, 30,30);
        ellipse(this.x - 41, this.y - 41, 25,25);
        ellipse(this.x - 56, this.y - 56, 20,20);
        pop();

        push();
        fill(this.col5);
        rotate(this.minusVinkel + 350);
        ellipse(this.x, this.y, 35,35);
        ellipse(this.x - 22, this.y - 22, 30,30);
        ellipse(this.x - 41, this.y - 41, 25,25);
        ellipse(this.x - 56, this.y - 56, 20,20);
        pop();

        ////////////////////////////////////////////////////////////////////////////
    }

    move() {
        this.minusVinkel -= 4
    }
}