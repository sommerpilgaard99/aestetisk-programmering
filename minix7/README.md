## minix7

Link til program: https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix7/index.html

Link til koden: https://gitlab.com/sommerpilgaard99/aestetisk-programmering/-/blob/main/minix7/minix7.js

Link til Vinklen: https://gitlab.com/sommerpilgaard99/aestetisk-programmering/-/blob/main/minix7/Vinkel.js

Link til Regnbuen: https://gitlab.com/sommerpilgaard99/aestetisk-programmering/-/blob/main/minix7/Regnbue.js

### Hvilken minix?
Til denne minix fik vi stillet til opgave at gå tilbage og kigge på en af vores tidligere minix opgaver og vælge en som vi så skullelave om på og forbedre. Jeg valgte at lave om på min minix3, da jeg dengang lavede en meget lang kode, som var på omkrin 1600 linjer, selvom det meget var det samme som stod på hver linje, og nu da jeg så denne opgave beskrivelse, tænkte jeg at jeg nu havde de rigtige evner til at gøre noget ved det rod jeg lavede dengang.

### Ændringerne
Som jeg nævnte før ville jeg rode lidt op i min kode, og jeg tænkte at det ville være oplagt at gøre brug af noget af det nyeste vi har lært _Classes_, som er en løsning der passer perfekt til det problem jeg havde. Det er nemlig her man får mange objekter ind under en class så man kan "spawn" mange af dem direkte i sin sketch.

![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix7/CD.jpg)

Jeg delte mine _ellipser()_ op i 2 classes, _Regnbuen_ som snurrer imod uret og _Vinkel_ som snurrer med uret.
Herefter kiggede jeg tilbage til min minix3 sketch og fandt koordinaterner og størrelsesforholdene for den yderste og største ellipse, af den første række. Dette blev så koordinaterner for _this.x osv_

![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix7/Regnbue.jpg)

Jeg brugte også her _this.col_ 1 til 5, til at lave parametrene for hvilke farver hver række skulle have, så det var nemmere i _show()_ bare at ændre col varianten.

![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix7/Regnbue1.jpg)

Her er en af mine fulde rækker i min _show()_, da jeg inde i min sketch har _translate()_, skal jeg her bruge `this.x -` & `this.y -` ellers starter de fra hjørnet.
Herefter var det simpelt, jeg skulle bare copy paste og ændre col til den næste farve, og ændre starts vinklen ved at sige `this.minusVinkel - 10` og så bare repeat til jeg har alle dem som skal bruges.

Jeg fik min kode fra de 1600 til 500, hvor min sketch nu kun er 40 linjer.
Jeg kunne inkorporere meget af det nye vi har haft siden jeg dengang lavede min minix3, men det var faktisk det her med det objektsorienterede programmering som havde størst betydning for mig, da jeg første gang lavede denne throbber, havde jeg aldrig tænkt det som værende et objekt, men mere som flere objekter der tilsammen gav en helhed, men nu med de her classes har jeg kunne skabe den helhed helt ned til det dybeste niveau.

### Æstetisk programmering & digital kultur
Jeg synes det er bedst forklaret som det står skrevet i bogen 

_"computational culture is not just a trendy topic to study to improve problem-solving and analytical skills, or a way to understand more about what is happening with computational processes, but is a means to engage with programming to question existing technological paradigms and further create changes in the technical system."_

Det er ud fra dette, hvad jeg har læst og fået af vide til undervisningen, at jeg ville sige relationen, ligger i de reflektioner vi gør os når vi har kodet, de spørgsmål som vi stiller os selv. Vi laver koden og lærer alt det tekniske bagved, men i æstetisk programmering er det ikke her vi lukker den, vi gør som quoted siger, vi inteagere med programmerne og stiller spørgsmål til det som vi allerede kender om teknologien vi ser i virkeligheden. Dette er i min mening også grunden til at alle opgaver vi får stillet altid har roden i noget meget basalt, som vi nok aldrig har tænkt over før. Tag fx. vores minix4 som havde noget med data at gøre, jeg havde for eksempel aldrig rigtig tænkt over hvad de mindste valg vi tager på forskellige sider, kan have af konsekvenser, vi aldrig havde en idé om eksisterede. 

Når det så kommer til min valgte minix her, kan jeg desværre meddele at den ikke har haft ligeså meget at gøre med hvad jeg lige har nævnt om at vi stiller os spørgene om alt det vi kender, fra den teknologiske verde. Dette skyldes at da jeg lavede den tilbage første gang, havde jeg ikke helt fanget hvad grunden var til at vi reflekterede som vi gjorde, og det er først efter at jeg har lært hvad nytte det har. Når det så er sagt vil jeg alligevel gøre mit bedste for at lave en kopling, og jeg mener at den kan laves idet, at jeg har lavet en CD som skal symboliserer en tid før SSD og den hurtige processing vi kender i dag. CD'en viser en tid, hvor man altid skulle bruge en disc for at kunne køre et program og det altid tog uendeligt lang tid.
