# MiniX 1 Pizza


For at starte ud ville det være bedst at tjekke koden køre, og herefter kan man henvende sig til skærmbilledet under her.
[Tryk her for at se koden](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix1/index.html)

![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix1/Pizza_Minix.png)

Som man kan se udfra billedet og koden har jeg lavet en **pizza**, som ser bedre ud en den ville hvis jeg lavede en i virkeligheden. Jeg er meget tilfreds med hvordan det færdige produkt blev, selvom jeg undervejs stødte ind i flere problemer end jeg havde regnet med da jeg startede ud, dette vil jeg skrive mere om senere.
Da vi blev forklaret om denne første _MiniX_ fik vi afvide, at vi skulle udvælge en reference fra p5's hjemmeside, som vi gerne ville lære om. Så jeg startede ud ved at kigge på deres referenceliste, og tjekke mange forskellige ud, indtil jeg fald over den som er kaldet _ellipseMode_ og når man åbner den på deres hjemmeside bliver man mødt med en ellipse med en større ellipse rundt om. Idet jeg så dette tænkte jeg som det første på en pizza med en kant og havde allerede der besluttet mig om hvad jeg gerne ville begive mig ud i at lave.

#### _Nu vil jeg beskrive min proces med at lave min pizza form og hvad jeg lærte løbende i den proces._

##### _Pizzabunden_
Som sagt startede jeg ud med at forsøge mig i at bruge _ellipseMode_ til at lave en pizzabund, så jeg læste op på hvad man skulle gøre under referencen.  
_Pizzabund_

![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix1/Pizzabund_minix1.png)
Da jeg startede ud på at forsøge mig i at benytte _ellipseMode_ fandt jeg hurtigt ud af at man skulle skrive **RADIUS** & **CENTER** med caps, for ellers ville koden ikke længere virker, og kun min baggrund stod tilbage. I mit forsøg på at få det til at ligne en pizzabund, legede jeg rundt med størrelsesforholdene, indtil den havde min ønskede tykkelse. Derefter brugte jeg _fill_ til at lave ellipserne to forskellige nuancer af brun, som man ville associere med pizzaer.

##### _Baggrund_
Efter at have lavet min pizzabund var jeg ikke længere tilfreds med den kedelige baggrund som vi havde lært at lave i timen førhen, derfor forsøgte jeg mig i at lave et mønster i baggrunden, så den ikke var så monoton mere.

_Baggrund_
![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix1/Baggrund_minix1.png)
Her brugte jeg nye koder fra referencelisten, _strokeWeight_ sammen med _stroke_, som jeg benyttede til at skabe nogle tykke hvide linjer, som kunne laves til et firkantet mønster, der løb gennem hele baggrunden. Det var nemt nokat få linjerne til at sidde hvor jeg ønskede de skulle, da jeg bare kopierede alle de resterende udfra den første, da jeg havde placeret den rigtigt.

##### _Pizza skæring_
Jeg kom frem til at det ville være pænest hvis man kunne se at pizzaen allerede var skåret.
_Pizza skæring_
![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix1/Pizza_sk%C3%A6ring.png)
Så jeg benyttede mig af koden _line_ til at lave nogle diagonale, vandrette og lodrette linjer igennem pizzaen. Her lærte jeg at det var svært for mig at vide præcist hvilke koordinater jeg skulle skrive ind for at få den ønskede linje fra kant til kant. Derfor benyttede jeg her bare noget trial and error indtil jeg ramte rigtig.

##### _Topping_
Min pizza skulle selvfølgelig også have nogle toppings.

_Oliven_
![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix1/Oliven_minix1.png)
Som man kan se her benyttede jeg nogle simple ellipser og gjorde dem en smule bredere end høje, for at man kunne se at det var oliven og ikke bare sorte pletter. De blev placeret tilfældigt på pizzaen.

_Pepperoni_
![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix1/Pepperoni_minix1.png)
Pepperoni'erne blev også lavet af simple ellipser og blev også placeret tilfældigt på pizzaen.

##### _Kniv og Gaffel_
Jeg syntes der manglede noget ekstra så det ikke bare var en pizza med en baggrund, så jeg lavede også bestik ved siden af.
_Kniv og Gaffel_
![](https://sommerpilgaard99.gitlab.io/aestetisk-programmering/minix1/Kniv_og_gaffel.png)
Jeg benyttede samme trick her som jeg gjorde med linjerne til baggrunden, og brugte til sidst en ellipse til at lave knivets klinge.
