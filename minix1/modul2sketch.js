function setup() {
  // put setup code here
  createCanvas(500,500);  
  background (255,50,0);

  // Baggrundsmønster
  push();
  strokeWeight(5.0);
  stroke(255);
  line(50,0,50,500);
  line(100,0,100,500);
  line(150,0,150,500);
  line(200,0,200,500);
  line(250,0,250,500);
  line(300,0,300,500);
  line(350,0,350,500);
  line(400,0,400,500);
  line(450,0,450,500);
  line(0,50,500,50);
  line(0,100,500,100);
  line(0,150,500,150);
  line(0,200,500,200);
  line(0,250,500,250);
  line(0,300,500,300);
  line(0,350,500,350);
  line(0,400,500,400);
  line(0,450,500,450);
pop();

  print("Hi peeps");
}

function draw() {
  // put drawing code here

  //Pizzabund og kant
  push();
  strokeWeight(5.0);
  ellipseMode(RADIUS);
  fill(245,222,173);
 ellipse(250,250, 150,150);

  ellipseMode(CENTER);
  fill(255,204,0);
 ellipse(250,250, 250,250);
 pop();

 // Pepperoni
 push();
 strokeWeight(5.0);
 fill(255,0,0);
 ellipse(190,200,50);
 ellipse(256,330,50);
 ellipse(190,290,50);
 ellipse(320,270,50);
 ellipse(290,200,50);
 pop();
 
 // Oliven
push();
strokeWeight(5.0);
fill(0);
ellipse(210,150,20,15);
ellipse(310,210,20,15);
ellipse(260,260,20,15);
ellipse(230,350,20,15);
ellipse(170,270,20,15);
ellipse(310,340,20,15);
pop();

// Pizza skæring
push();
strokeWeight(5.0);
 line(144, 144, 356,356);
 line(250, 100, 250, 400);
 line(100, 250, 400, 250);
 line(144, 356, 356, 144);
pop();

// Kniv og Gaffel
push();
 strokeWeight(12.0);
strokeCap(ROUND);
stroke(150);
line(50, 200, 50, 350);
line(450, 250, 450, 350);
line(425, 240, 475, 240);
line(425, 240, 425, 200);
line(450, 240, 450, 200);
line(475, 240, 475, 200);
ellipse(45,245,10,90);
pop();
}
